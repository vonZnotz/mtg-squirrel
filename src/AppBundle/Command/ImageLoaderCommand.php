<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImageLoaderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('mtgsquirrel:imageLoader');
        $this->setDescription('');
        $this->setHelp('sample usage: php app/console mtgsquirrel:imageLoader
            \'https://deckbox.org/system/images/mtg/cards/%s.jpg\'
            \'/home/tneumann/.local/share/data/Cockatrice/Cockatrice/cards.xml\'
            \'/home/tneumann/.local/share/data/Cockatrice/Cockatrice/pics/downloadedPics/%s/%s.jpg\'');
        $this->addArgument(
            'downloadPattern',
            InputArgument::REQUIRED,
            'example: http://domain.com/!%s!.jpg -> %s = cardid'
        );
        $this->addArgument(
            'inputXmlPath',
            InputArgument::REQUIRED,
            '/path/xml/cards.xml'
        );
        $this->addArgument(
            'targetDownloadPathPattern',
            InputArgument::REQUIRED,
            'example: /path/to/images/pics/downloadedPics/%s/%s.jpg -> %s = set shortcut'
        );
        $this->addOption('overwrite', 'o', InputOption::VALUE_OPTIONAL, 'overwrite already downloaded images');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $imageLoader = $this->getContainer()->get('app.service.image_loader_service');
        $imageLoader->setOverwrite($input->getOption('overwrite'));
        $imageLoader->setCommandOutput($output);
        $imageLoader->setDownloadPattern($input->getArgument('downloadPattern'));
        $imageLoader->setTargetDownloadPathPattern(
            $input->getArgument('targetDownloadPathPattern')
        );
        $imageLoader->setInputXmlPath($input->getArgument('inputXmlPath'));
        $imageLoader->loadImages();
    }
}
