<?php

namespace AppBundle\Command;

use SensioLabs\Security\Crawler\CurlCrawler;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('mtgsquirrel:import');
        $this->setDescription('');
        $this->addArgument(
            'name',
            InputArgument::REQUIRED,
            'Who do you want to greet?'
        )
            ->addOption(
                'yell',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will yell in uppercase letters'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $locator = new FileLocator(__DIR__ . '/../Resources/config');
        $yamlConfigFile = $locator->locate('config.yml');
        $config = Yaml::parse($yamlConfigFile);
        $downloadUrl = $config['import']['download_url'];
        var_dump($downloadUrl);
        return;


        $curlCrawler = new CurlCrawler();
        $output->writeln();
    }

}
