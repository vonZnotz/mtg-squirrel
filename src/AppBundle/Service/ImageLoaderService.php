<?php

namespace AppBundle\Service;

use Buzz\Browser;
use Buzz\Client\Curl;
use Buzz\Exception\RequestException;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ImageLoaderService
{
    /**
     * The download patter, where to find the images
     * e.g.: http://domain.com/!%s!.jpg
     * @var string
     */
    private $downloadPattern;

    /**
     * The target path pattern, where to put the images
     * e.g.: /path/to/images/pics/downloadedPics/%s/%s.jpg
     *
     * 1st %s = set
     * 2nd %s = cardname
     *
     * @var string
     */
    private $targetDownloadPathPattern;

    /**
     * The input xml file with the cards
     * @var string
     */
    private $inputXmlPath;

    /** @var bool */
    private $overwrite;

    /** @var OutputInterface */
    private $commandOutput;

    /** @var Browser */
    private $browser;

    /** @var int */
    private $counter = 0;

    public function loadImages()
    {
        $xmlContent = file_get_contents($this->inputXmlPath);
        $curl = new Curl();
        $this->browser = new Browser();
        $this->browser->setClient($curl);

        $crawler = new Crawler($xmlContent);

        $crawler->filterXPath('//cockatrice_carddatabase/cards/card')->each(function($cardItem)  {
            /** @var Crawler $cardItem */
            $nameNode = $cardItem->filter('name');
            $name = $nameNode->text();
            $cardItem->filter('set')->each(function($setItem) use ($name) {
                $this->counter++;
                if ($this->counter % 100 == 0) {
                    $this->commandOutput->writeln("download counter: " . $this->counter);
                }

                /** @var Crawler $setItem */
                $multiverseId = $setItem->attr('muId');
                $downloadUrl = sprintf($this->downloadPattern, $multiverseId);
                $targetFile = sprintf($this->targetDownloadPathPattern, $setItem->text(), $name);

                if (
                    in_array(
                        $name,
                        array(
                            'Who // What // When // Where // Why',
                            'Our Market Research Shows That Players Like Really Long Card Names So We Made this Card to Have the Absolute Longest Card Name Ever Elemental'
                        )
                    )
                ) {
                    $this->commandOutput->writeln("invalid filename: '$targetFile'");
                    return true;
                }

                $targetFolder = substr($targetFile, 0, strrpos($targetFile, '/'));
                if (!is_dir($targetFolder)) {
                    @mkdir($targetFolder);
                }

                if (($this->overwrite && file_exists($targetFile)) || !file_exists($targetFile)) {
                    touch($targetFile);
                    $this->commandOutput->writeln($targetFile);
                    $this->commandOutput->writeln('downloading: ' . $downloadUrl);

                    $response = null;
                    while ($response === null) {
                        try {
                            $response = $this->browser->get($downloadUrl);
                        } catch (RequestException $e) {
                            $this->commandOutput->writeln('timeout...trying again (break by hand to get out)');
                        }
                    }
                    $this->commandOutput->writeln('to:' . $targetFile);
                    file_put_contents($targetFile, $response->getContent());
                }
            });
        });


    }

    /**
     * @param string $downloadPattern
     * @return ImageLoaderService
     */
    public function setDownloadPattern($downloadPattern)
    {
        $this->downloadPattern = $downloadPattern;
        return $this;
    }

    /**
     * @param string $targetDownloadPathPattern
     * @return ImageLoaderService
     */
    public function setTargetDownloadPathPattern($targetDownloadPathPattern)
    {
        $this->targetDownloadPathPattern = $targetDownloadPathPattern;
        return $this;
    }

    /**
     * @param string $inputXmlPath
     * @return ImageLoaderService
     */
    public function setInputXmlPath($inputXmlPath)
    {
        $this->inputXmlPath = $inputXmlPath;
        return $this;
    }

    /**
     * @param OutputInterface $commandOutput
     * @return ImageLoaderService
     */
    public function setCommandOutput($commandOutput)
    {
        $this->commandOutput = $commandOutput;
        return $this;
    }

    /**
     * @param boolean $overwrite
     */
    public function setOverwrite($overwrite)
    {
        $this->overwrite = $overwrite;
    }
}
